package com.aregevdev.firebaseapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    TextView textViewDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        textViewDetails = findViewById(R.id.text_view_details);
        StringBuilder stringBuilder = new StringBuilder()
            .append("User ID: ").append(user.getUid()).append("\n")
            .append("Username: ").append(user.getDisplayName()).append("\n")
            .append("Email: ").append(user.getEmail()).append("\n");

        textViewDetails.setText(stringBuilder);
    }

    @Override
    public void onBackPressed() {
        FirebaseAuth.getInstance().signOut();
        super.onBackPressed();
    }
}
